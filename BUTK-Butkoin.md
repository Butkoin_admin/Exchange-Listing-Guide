# Butkoin - BUTK

### 文档版本：0.12.15.2
检查日期: 2021.04.19

### 官网地址：
https://butcoin.tech/

### 钱包下载：
https://github.com/Butkoin/but/releases/tag/v1.2.15.2

### 开发文档
https://butcoin.tech/en/doc/0.12.15.1/

### 安装说明：
1、解压缩文件
```
tar -xvf but-1.2.15.2-x86_64-linux-gnu.tar.gz
```

2、创建服务
```
vi /usr/lib/systemd/system/but.service
```
```
[Unit]
Description=Butkoin

[Service]
Type=forking

ExecStart=/安装目录/bin/butd -daemon -conf=/数据目录/but.conf
ExecStop=/安装目录/bin/but-cli -stop -conf=/数据目录/but.conf

[Install]
WantedBy=multi-user.target
```

### 配置文件：
```
vi /数据目录/but.conf
```
```
bind=127.0.0.1              #绑定端口，建议127.0.0.1
server=1                    
txindex=1
datadir=/but-data           #数据存储目录

rpcbind=127.0.0.1           #RPC端口，建议127.0.0.1
rpcport=9998
rpcuser=                    #RPC用户名
rpcauth=                    #RPC认证信息
rpcallowip=                 #允许访问的IP
```

### 升级说明：
1、下载最新的版本  
2、重命名旧版本的程序目录以做备份  
3、解压缩新的压缩包并改名为but  
4、重启服务
```
systemctl restart but
```
5、观察debug.log是否正常
```
tail -f /数据目录/debug.log
```

### 创建地址：
1、创建地址
```
curl -X POST -H 'content-type: text/plain;' http://127.0.0.1:9998/ -u RPC用户:RPC密码 \
-d '{"jsonrpc":"1.0","id":"随便写","method":"getnewaddress","params":[]}'  
```
2、获取私钥
```
curl -X POST -H 'content-type: text/plain;' http://127.0.0.1:9998/ -u RPC用户:RPC密码 \
-d '{"jsonrpc":"1.0","id":"随便写","method":"dumpprivkey","params":["But地址"]}'
```

### 追踪入账：
1、获取最新的区块高度
```
curl -X POST -H 'content-type: text/plain;' http://127.0.0.1:9998/ -u RPC用户:RPC密码 \
-d '{"jsonrpc":"1.0","id":"随便写","method":"getblockcount","params":[]}'  
```
2、根据区块高度获取区块Hash
```
curl -X POST -H 'content-type: text/plain;' http://127.0.0.1:9998/ -u RPC用户:RPC密码 \
-d '{"jsonrpc":"1.0","id":"随便写","method":"getblockhash","params":[区块高度]}'  
```
3、根据区块Hash获取交易列表
```
curl -X POST -H 'content-type: text/plain;' http://127.0.0.1:9998/ -u RPC用户:RPC密码 \
-d '{"jsonrpc":"1.0","id":"随便写","method":"getblock","params":["区块Hash"]}'  
```
4、根据TxId获取具体信息
```
curl -X POST -H 'content-type: text/plain;' http://127.0.0.1:9998/ -u RPC用户:RPC密码 \
-d '{"jsonrpc":"1.0","id":"随便写","method":"gettransaction","params":["交易TxId"]}'  
```

### 对外提币：
1、估算当前费用
```
curl -X POST -H 'content-type: text/plain;' http://127.0.0.1:9998/ -u RPC用户:RPC密码 \
-d '{"jsonrpc":"1.0","id":"随便写","method":"estimatesmartfee","params":[期望多少个区块内到达]}'  
```

2、创建RAW交易
```
curl -X POST -H 'content-type: text/plain;' http://127.0.0.1:9998/ -u RPC用户:RPC密码 \
-d '{"jsonrpc":"1.0","id":"随便写","method":"createrawtransaction","params":[[{"txid":"输出的Txid","vout":输出的Tx的序号}],[{"接收地址":接收金额}]}'  
```

3、签名RAW交易
```
curl -X POST -H 'content-type: text/plain;' http://127.0.0.1:9998/ -u RPC用户:RPC密码 \
-d '{"jsonrpc":"1.0","id":"随便写","method":"signrawtransactionwithkey","params":["创建RAW返回的Hex",["相关输出地址的私钥"]]}'  
```

4、发送RAW交易
```
curl -X POST -H 'content-type: text/plain;' http://127.0.0.1:9998/ -u RPC用户:RPC密码 \
-d '{"jsonrpc":"1.0","id":"随便写","method":"sendrawtransaction","params":["完成签名的Hex"]}'  
```

### 归集处理：
* But无需归集

### 灾难恢复：
* 用户私钥加密后另外存储，以做备份，灾难发生时重建节点即可

### 注意事项：
